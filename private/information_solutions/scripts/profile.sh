#!/bin/bash

curl -s https://support-sp.apple.com/sp/product?cc=$(
  system_profiler SPHardwareDataType \
    | awk '/Serial/ {print $4}' \
    | cut -c 9-
) | sed 's|.*<configCode>\(.*\)</configCode>.*|\1|'

echo -ne "\nSerial: "; ioreg -c IOPlatformExpertDevice -d 2 | awk -F\" '/IOPlatformSerialNumber/{print $(NF-1)}'

system_profiler SPHardwareDataType | grep "Model Identifier" | sed 's/^      //g'


system_profiler SPHardwareDataType | grep -i 'model\|Processor\|Cores\|Memory' | sed 's/^      //g'

diskutil list internal physical  | grep disk[0-9]$ | awk '{{print "Internal Disk Size: "$3" "$4}}'

ifconfig en0 | grep ether | sed 's/^\tether /Mac Address\:  /'


model=$(system_profiler SPHardwareDataType | grep "Model Identifier" | sed 's/     Model Identifier\: //g' | awk '{{print $1}}')

curl https://di-api.reincubate.com/v1/apple-identifiers/$model/
